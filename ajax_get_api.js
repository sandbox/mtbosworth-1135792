
// Makes sure drupal object is there without overwriting
var Drupal = Drupal || {};

// Define module namespace
Drupal.AjaxGetAPI = {

	parseViewDomId : function(str) {
		// parse view-dom-id-# class
		var domIdClass = str.match(/view-dom-id-\d{1,10}/g)[0];
		
		// parse id
		var domId = domIdClass.match(/\d{1,10}/g)[0];
		
		// parse integer
		return parseInt(domId,10);
	},
	
	updateView : function(view_name,args,view_id){
		// initial args
		var args = args || [];
		
		// initial view_id, parse if not passed
		var view_id = view_id || this.parseViewDomId($('.view-id-' + view_name + ':first').attr('class'));
			
		// select view
		var view = $('.view-dom-id-' + view_id + ':first');
		
		// request new view data
		$.ajax({url:'/ajax-get/view/' + view_name + '/' + args.join('/'), success:function(html){
			
			// new data will have the wrong view dom id, so update
			var result = $(html).removeClass('view-dom-id-1').addClass('view-dom-id-' + view_id);
			
			// swap out current view with new html 
			view.replaceWith(result);
			
			// Reattach behaviors
			Drupal.attachBehaviors(view);
		}});
		
	}
	
};

